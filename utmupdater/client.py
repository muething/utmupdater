import requests
from .util import info
from .certificate import Certificate
from contextlib import contextmanager
from datetime import datetime, timezone

class Client:

    def __init__(self,host,token,verify=None):
        self.host = host
        self.baseurl = host + '/api'
        self.token = token
        self.connected = False
        self.session = requests.Session()
        self.session.auth = 'token',token
        self.session.headers.update({
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            })
        if verify:
            self.session.verify = verify

    def url(self,*path):
        return '/'.join((self.baseurl,*path))

    def connect(self):
        self.session.get(self.url()).raise_for_status()
        self.connected = True

    def disconnect(self):
        if self.connected:
            self.session.get(self.url(),headers={'X-Restd-Session':'close'}).raise_for_status()
        self.connected = False

    def list_certificates(self,type='ca/host_key_cert'):
        r = self.get(type)
        r.raise_for_status()
        return r.json()

    def get_certificate(self,ref,type='ca/host_key_cert'):
        r = self.get(type,ref)
        r.raise_for_status()
        data = r.json()
        r = self.get('ca/meta_x509',data['meta'])
        r.raise_for_status()
        meta = r.json()
        return Certificate.from_server(data,meta)

    def patch_certificate(self,cert,data_changes,meta_changes):
        info('Patching certificate data for {}',cert.name)
        self.patch(cert.type,cert.ref,json=data_changes).raise_for_status()
        info('Patching certificate meta data for {}',cert.name)
        self.patch('ca/meta_x509',cert.meta_ref,json=meta_changes).raise_for_status()


    def update_certificate_from_files(self,old_cert,certfile,keyfile=None):

        new_cert = Certificate.from_file('ca/host_key_cert',certfile,keyfile)
        new_cert.ref = old_cert.ref

        if old_cert.fingerprint == new_cert.fingerprint:
            info('Certificate already up to date, fingerprint: {}',old_cert.fingerprint)
            return False

        data_patch = {
            'certificate' : new_cert.certificate,
            'comment' : 'automatically managed by utmupdater, last update: {}'.format(datetime.now(timezone.utc))
        }

        if old_cert.key and old_cert.key != new_cert.key:
            data_patch['key'] = new_cert.key

        meta_patch = {
            'startdate' : new_cert.startdate,
            'enddate' : new_cert.enddate,
            'subject' : new_cert.subject,
            'subject_hash' : new_cert.subject_hash,
            'subject_alt_names' : new_cert.subject_alt_names,
            'issuer' : new_cert.issuer,
            'issuer_hash' : new_cert.issuer_hash,
            'fingerprint' : new_cert.fingerprint,
            'serial' : new_cert.serial,
            'public_key_algorithm' : new_cert.public_key,
            'vpn_id' : new_cert.vpn_id,
            'vpn_id_type' : new_cert.vpn_id_type
        }

        self.patch_certificate(old_cert,data_patch,meta_patch)
        info('Certificate updated, old fingerprint: {}, new fingerprint: {}',old_cert.fingerprint,new_cert.fingerprint)
        return True

    def get(self,*target):
        r = self.session.get(self.url('objects',*target))
        r.raise_for_status()
        return r

    def post(self,*target,json=None):
        r = self.session.post(self.url('objects',*target),json=json)
        r.raise_for_status()
        return r

    def patch(self,*target,json=None):
        r = self.session.patch(self.url('objects',*target),json=json)
        r.raise_for_status()
        return r

@contextmanager
def connect(host,token,verify=None):
    client = Client(host,token,verify)
    client.connect()
    try:
        yield client
    except:
        client.disconnect()
        raise
