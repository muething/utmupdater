import click
from .util import info
from .client import connect
import pkg_resources
from pathlib import Path
from lya import AttrDict

debug_mode = False

def main():
    try:
        import sys
        cli.main(sys.argv[1:],standalone_mode=False)
        sys.exit()
    except Exception as e:
        info('ERROR: {}',e)
        if debug_mode:
            raise
        sys.exit(1)

@click.group()
@click.option('--debug/--nodebug',default=False)
@click.option('--api-ca-bundle',
              type=click.Path(exists=True),
              help='Path to a file containing intermediate certificates required to connect to the UTM API endpoint.')
@click.version_option(version=pkg_resources.get_distribution('utmupdater').version)
def cli(debug,api_ca_bundle):
    global debug_mode
    debug_mode = debug


@cli.command()
@click.pass_context
@click.option('--host','-h',
              metavar='URL',
              required=True,
              help="UTM that the script will operator on. Should be specified as the URL to the normal Webadmin login, including the port.")
@click.option('--token','-p',
              prompt=True,hide_input=True,
              metavar='TOKEN',
              required=True,
              help="API token for authenticating to the UTM.")
@click.option('--type','-t',
              default='ca/host_key_cert',
              help='Type of certificates to list, e.g. ca/host_key_cert.')
def list(ctx,host,token,type):
    ca_bundle = ctx.parent.params['api_ca_bundle']
    with connect(host,token,verify=ca_bundle) as client:
        for cert in client.list_certificates(type):
            click.echo('{}\t{}'.format(cert['name'],cert['_ref']))



@cli.command()
@click.pass_context
@click.option('--host','-h',
              metavar='URL',
              required=True,
              help="UTM that the script will operator on. Should be specified as the URL to the normal Webadmin login, including the port.")
@click.option('--token','-p',
              prompt=True,hide_input=True,
              metavar='TOKEN',
              required=True,
              help="API token for authenticating to the UTM.")
@click.argument('ref')
@click.argument('certificate',type=click.Path(exists=True))
@click.argument('key',type=click.Path(exists=True))
@click.argument('intermediate',type=click.Path(exists=True))
def update(ctx,ref,certificate,key,intermediate):
    ca_bundle = ctx.parent.params['api_ca_bundle']
    with connect(host,token,verify=ca_bundle) as client:
        old_cert = client.get_certificate(ref)
        if client.update_certificate_from_files(old_cert,certificate,key):
            old_ca = client.get_certificate(old_cert.ca_ref,type='ca/verification_ca')
            client.update_certificate_from_files(old_ca,intermediate)


@cli.command()
@click.pass_context
@click.option('--host','-h',
              metavar='URL',
              required=True,
              help="UTM that the script will operator on. Should be specified as the URL to the normal Webadmin login, including the port.")
@click.option('--token','-p',
              prompt=True,hide_input=True,
              metavar='TOKEN',
              required=True,
              help="API token for authenticating to the UTM.")
@click.argument('obj_path')
@click.argument('json',type=click.File())
def patch(ctx,obj_path,json):
    ca_bundle = ctx.parent.params['api_ca_bundle']
    with connect(host,token,verify=ca_bundle) as client:
        client.patch(obj_path,json=json.read())


@cli.command()
@click.pass_context
@click.argument('target_dir',type=click.Path(exists=True,file_okay=False))
@click.argument('certificate',type=click.Path(exists=True))
@click.argument('key',type=click.Path(exists=True))
@click.argument('intermediate',type=click.Path(exists=True))
def batch(ctx,target_dir,certificate,key,intermediate):
    ca_bundle = ctx.parent.params['api_ca_bundle']

    for target in Path(target_dir).glob('*.conf'):
        config = AttrDict.from_yaml(str(target))
        info('Updating certificate on {}',config.host)
        with connect(config.host,config.token,verify=ca_bundle) as client:
            old_cert = client.get_certificate(config.ref)
            if client.update_certificate_from_files(old_cert,certificate,key):
                old_ca = client.get_certificate(old_cert.ca_ref,type='ca/verification_ca')
                client.update_certificate_from_files(old_ca,intermediate)
