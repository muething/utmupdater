import re
from subprocess import check_output

startdate_re = re.compile(r'not before\s*:\s*(\S.*)',re.I)
enddate_re = re.compile(r'not after\s*:\s*(\S.*)',re.I)
issuer_re = re.compile(r'issuer\s*:\s*(\S.*)',re.I)
subject_re = re.compile(r'subject\s*:\s*(\S.*)',re.I)
serial_re = re.compile(r'Serial Number\s*:.*\n\s*([0-9a-f:]+)',re.I | re.M)
pub_key_re = re.compile(r'Public Key Algorithm\s*:\s*(\S.*)',re.I)
subject_alt_name_re = re.compile(r'X509v3 Subject Alternative Name\s*:.*\n\s*([^\n]+)',re.I | re.M)

class Certificate:

    @staticmethod
    def query(path,*args):
        return check_output(['openssl','x509','-in',path,'-noout',*args],universal_newlines=True)

    @classmethod
    def from_file(cls,type,certfile,keyfile=None):
        subject_hash = cls.query(certfile,'-subject_hash').strip()
        issuer_hash = cls.query(certfile,'-issuer_hash').strip()
        fingerprint = cls.query(certfile,'-fingerprint').split('=')[1].strip()
        text = cls.query(certfile,'-text')
        startdate = startdate_re.search(text).group(1)
        enddate = enddate_re.search(text).group(1)
        issuer = issuer_re.search(text).group(1)
        subject = subject_re.search(text).group(1)
        serial = serial_re.search(text).group(1)
        pub_key = pub_key_re.search(text).group(1)
        try:
            subject_alt_names = re.split(', ',subject_alt_name_re.search(text).group(1))
        except:
            subject_alt_names = []
        vpn_id = subject
        vpn_id_type = 'fqdn'
        certdata = text + open(certfile).read()
        if keyfile:
            private_key = open(keyfile).read()
        else:
            private_key = None
        cert = cls(
            type,
            None,
            certdata,
            subject_hash,
            issuer_hash,
            fingerprint,
            startdate,enddate,
            issuer,
            subject,
            serial,
            pub_key,
            subject_alt_names,
            vpn_id,
            vpn_id_type,
            private_key)
        cert.text = text
        return cert

    @classmethod
    def from_server(cls,data,meta):
        cert = cls(
            data['_type'],
            data['_ref'].strip(),
            data['certificate'],
            meta['subject_hash'].strip(),
            meta['issuer_hash'].strip(),
            meta['fingerprint'].strip(),
            meta['startdate'],
            meta['enddate'],
            meta['issuer'],
            meta['subject'],
            meta['serial'].strip(),
            meta['public_key_algorithm'].strip(),
            meta['subject_alt_names'],
            meta['vpn_id'],
            meta['vpn_id_type']
            )
        try:
            cert.ca_ref = data['ca'].strip()
        except KeyError:
            cert.ca_ref = None
        try:
            cert.key = data['key']
        except KeyError:
            cert.key = None
        cert.meta_ref = data['meta'].strip()
        cert.meta = meta
        cert.data = data
        cert.name = data['name']
        return cert

    def __init__(self,type,ref,certdata,subject_hash,issuer_hash,fingerprint,
                 startdate,enddate,issuer,subject,serial,pub_key,subject_alt_names,
                 vpn_id,vpn_id_type,private_key=None):
        self.type = type
        self.ref = ref
        self.certificate = certdata
        self.subject_hash = subject_hash
        self.issuer_hash = issuer_hash
        self.fingerprint = fingerprint
        self.startdate = startdate
        self.enddate = enddate
        self.issuer = issuer
        self.subject = subject
        self.serial = serial
        self.public_key = pub_key
        self.subject_alt_names = subject_alt_names
        self.vpn_id = vpn_id
        self.vpn_id_type = vpn_id_type
        self.key = private_key
