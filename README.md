# Library and tool for updating certificates on Sophos UTM firewalls

Sophos UTM firewalls expose a number of TLS-protected services, but right now it is not possible
to automatically provision new certificates for those endpoints with on-board tools.

This is a small utility and library that lets you do exactly that using the REST API introduced in
UTM 9.5, mostly intended for integration with Let's Encrypt.

It is largely based on older work at
https://gitlab.com/mbunkus/utm-update-certificate/blob/master/utm_update_certificate.pl, but that
code uses a shell login to perform the same task. In recent versions of UTM, this requires root
access which voids the warranty. Nevertheless, I mostly just copied a lot of useful information from
that project, so thank you!
