from setuptools import setup, PEP420PackageFinder
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='utmupdater',  # Required

    use_scm_version=True,

    description='A tool and library for updating host certificates on Sophos UTM firewalls.',  # Required

    long_description=long_description,  # Optional

    long_description_content_type='text/markdown',  # Optional (see note above)

    url='https://gitlab.com/muething/utmupdater',  # Optional

    author='Steffen Müthing',  # Optional

    author_email='steffen.muething@muething.com',  # Optional

    classifiers=[  # Optional
        'Development Status :: 4 - Beta',

        'Intended Audience :: Developers',

        'License :: OSI Approved :: BSD License',

        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
    ],

    packages=PEP420PackageFinder.find(),  # Required

    setup_requires=[
        'setuptools_scm',
    ],

    install_requires=[
        'requests',
        'click',
        'layered-yaml-attrdict-config',
    ],  # Optional

    extras_require={  # Optional
    },

    python_requires=">=3.6",

    package_data={  # Optional
    },

    # data_files=[('my_data', ['data/data_file'])],  # Optional

    entry_points={  # Optional
        'console_scripts': [
            'utmupdater=utmupdater.cli:main',
        ],
    },

    project_urls={  # Optional
        'Bug Reports': 'https://gitlab.com/muething/utmupdater/issues',
        'Source': 'https://gitlab.com/muething/utmupdater',
    },
)
